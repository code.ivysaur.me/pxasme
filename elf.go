package main

// Elf64_Ehdr is the main ELF header
type Elf64_Ehdr struct {
	e_ident     [16]byte
	e_type      uint16
	e_machine   uint16
	e_version   uint32
	e_entry     uint64
	e_phoff     uint64
	e_shoff     uint64
	e_flags     uint32
	e_ehsize    uint16
	e_phentsize uint16
	e_phnum     uint16
	e_shentsize uint16
	e_shnum     uint16
	e_shstrndx  uint16
}

// File types
const (
	ET_NONE = 0
	ET_REL  = 1
	ET_EXEC = 2
	ET_DYN  = 3
	ET_CORE = 4
)

// Elf64_Phdr is the Program Header
type Elf64_Phdr struct {
	p_type   uint32
	p_flags  uint32
	p_offset uint64
	p_vaddr  uint64
	p_paddr  uint64
	p_filesz uint64
	p_memsz  uint64
	p_align  uint64
}

const (
	SHT_NULL          = 0
	SHT_PROGBITS      = 1
	SHT_SYMTAB        = 2
	SHT_STRTAB        = 3
	SHT_RELA          = 4
	SHT_HASH          = 5
	SHT_DYNAMIC       = 6
	SHT_NOTE          = 7
	SHT_NOBITS        = 8
	SHT_REL           = 9
	SHT_SHLIB         = 10
	SHT_DYNSYM        = 11
	SHT_INIT_ARRAY    = 14
	SHT_FINI_ARRAY    = 15
	SHT_PREINIT_ARRAY = 16
	SHT_GROUP         = 17
	SHT_SYMTAB_SHNDX  = 18

	SHF_WRITE            = 0x1
	SHF_ALLOC            = 0x2
	SHF_EXECINSTR        = 0x4
	SHF_MERGE            = 0x10
	SHF_STRINGS          = 0x20
	SHF_INFO_LINK        = 0x40
	SHF_LINK_ORDER       = 0x80
	SHF_OS_NONCONFORMING = 0x100
	SHF_GROUP            = 0x200
	SHF_TLS              = 0x400
)

// Elf64_Shdr is the Section header
type Elf64_Shdr struct {
	sh_name      uint32
	sh_type      uint32
	sh_flags     uint64
	sh_addr      uint64
	sh_offset    uint64
	sh_size      uint64
	sh_link      uint32
	sh_info      uint32
	sh_addralign uint64
	sh_entsize   uint64
}

const (
	STB_LOCAL  = 0
	STB_GLOBAL = 1
	STB_WEAK   = 2

	STT_NOTYPE  = 0
	STT_OBJECT  = 1
	STT_FUNC    = 2
	STT_SECTION = 3
	STT_FILE    = 4
	STT_COMMON  = 5
	STT_TLS     = 6

	STV_DEFAULT   = 0
	STV_INTERNAL  = 1
	STV_HIDDEN    = 2
	STV_PROTECTED = 3
)

// Elf64_Sym is a symbol
type Elf64_Sym struct {
	St_name  uint32
	St_info  uint8
	St_other uint8
	St_shndx uint16
	St_value uint64
	St_size  uint64
}

// Elf64_Rela is a relocation with addend
type Elf64_Rela struct {
	R_offset uint64
	R_info   uint64
	R_addend int64
}

// Relocation types
type ElfRelocationType int

const (
	R_X86_64_NONE            ElfRelocationType = 0
	R_X86_64_64              ElfRelocationType = 1
	R_X86_64_PC32            ElfRelocationType = 2
	R_X86_64_GOT32           ElfRelocationType = 3
	R_X86_64_PLT32           ElfRelocationType = 4
	R_X86_64_COPY            ElfRelocationType = 5
	R_X86_64_GLOB_DAT        ElfRelocationType = 6
	R_X86_64_JUMP_SLOT       ElfRelocationType = 7
	R_X86_64_RELATIVE        ElfRelocationType = 8
	R_X86_64_GOTPCREL        ElfRelocationType = 9
	R_X86_64_32              ElfRelocationType = 10
	R_X86_64_32S             ElfRelocationType = 11
	R_X86_64_16              ElfRelocationType = 12
	R_X86_64_PC16            ElfRelocationType = 13
	R_X86_64_8               ElfRelocationType = 14
	R_X86_64_PC8             ElfRelocationType = 15
	R_X86_64_DTPMOD64        ElfRelocationType = 16
	R_X86_64_DTPOFF64        ElfRelocationType = 17
	R_X86_64_TPOFF64         ElfRelocationType = 18
	R_X86_64_TLSGD           ElfRelocationType = 19
	R_X86_64_TLSLD           ElfRelocationType = 20
	R_X86_64_DTPOFF32        ElfRelocationType = 21
	R_X86_64_GOTTPOFF        ElfRelocationType = 22
	R_X86_64_TPOFF32         ElfRelocationType = 23
	R_X86_64_PC64            ElfRelocationType = 24
	R_X86_64_GOTOFF64        ElfRelocationType = 25
	R_X86_64_GOTPC32         ElfRelocationType = 26
	R_X86_64_GOT64           ElfRelocationType = 27
	R_X86_64_GOTPCREL64      ElfRelocationType = 28
	R_X86_64_GOTPC64         ElfRelocationType = 29
	R_X86_64_GOTPLT64        ElfRelocationType = 30
	R_X86_64_PLTOFF64        ElfRelocationType = 31
	R_X86_64_SIZE32          ElfRelocationType = 32
	R_X86_64_SIZE64          ElfRelocationType = 33
	R_X86_64_GOTPC32_TLSDESC ElfRelocationType = 34
	R_X86_64_TLSDESC_CALL    ElfRelocationType = 35
	R_X86_64_TLSDESC         ElfRelocationType = 36
	R_X86_64_IRELATIVE       ElfRelocationType = 37
)
