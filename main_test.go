package main

import (
	// "io/ioutil"
	"os"
	"strings"
	"testing"
)

func TestCompile(t *testing.T) {

	// @ref https://gist.github.com/armicron/e891709ce8893df2fd5fc74c846dcf20
	const src = `
section .rodata
    $msg = sz "Hello, world\n"
    $filename = sz "test.txt"
    
section .data
    $fd = u64 0

section .text

global _start:         ;tell linker entry point
    mov rdi, &$filename
    mov rsi, 66        ;O_CREAT = 0102o (man open)
    mov rdx, 438       ;umode_t = 0666 octal
    mov rax, 2
    syscall

    mov $fd, rax
    mov rdx, strlen($msg) ;message strlen
    mov rsi, &$msg     ;message to write
    mov rdi, $fd       ;file descriptor
    mov rax, 1         ;system call number (sys_write)
    syscall            ;call kernel

    mov rdi, $fd
    mov rax, 3         ;sys_close
    syscall

    mov rax, 60        ;system call number (sys_exit)
    syscall            ;call kernel

`

	/*
		assemble(strings.NewReader(src), ioutil.Discard)
	*/

	fh, err := os.OpenFile("output.o", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}

	err = assemble(strings.NewReader(src), fh) // ioutil.Discard)
	if err != nil {
		panic(err)
	}

}
