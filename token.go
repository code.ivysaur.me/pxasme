package main

type Token interface{}

type SectionToken struct {
	SectionName string
}

type GlobalToken struct {
	IdentifierName string
}

type LabelToken struct {
	LabelName string
	IsGlobal  bool
}

type MovInstrToken struct {
	Args []string
}

type SyscallInstrToken struct{}

type RetInstrToken struct{}

type NopInstrToken struct{}

type DataVariableInstrToken struct {
	VarName   string
	Sizeclass string // sz, u8, u16, u32, u64
	Value     string
}
