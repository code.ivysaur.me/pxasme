package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
)

//

func main() {
	srcName := flag.String("Src", "-", "Input file (default: stdin)")
	destName := flag.String("Dest", "-", "Output file (default: stdout)")
	flag.Parse()

	var (
		err   error
		srch  io.Reader
		desth io.Writer
	)

	if *srcName == "-" {
		srch = os.Stdin
	} else {
		srch, err = os.OpenFile(*srcName, os.O_RDONLY, 0644)
		if err != nil {
			panic(err)
		}
	}

	if *destName == "-" {
		desth = os.Stdout
	} else {
		desth, err = os.OpenFile(*destName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
		if err != nil {
			panic(err)
		}
	}

	err = assemble(srch, desth)
	if err != nil {
		panic(err)
	}
}

func assemble(src io.Reader, dest io.Writer) error {
	lx := NewLexer(src)
	cc := NewCompiler()

mainloop:
	for {
		tok, err := lx.Next()
		if err != nil {
			if errors.Is(err, io.EOF) {
				// Reached EOF
				// Terminate compilation
				break mainloop
			}

			// Real error
			return fmt.Errorf("lexer: %w", err)
		}

		fmt.Printf("[line %d] %#v\n", lx.lineno, tok)

		err = cc.Compile(tok)
		if err != nil {
			return fmt.Errorf("compile: %w", err)
		}

	}

	err := cc.Finalize(dest)
	if err != nil {
		return fmt.Errorf("finalize: %w", err)
	}

	// Success
	return nil
}
