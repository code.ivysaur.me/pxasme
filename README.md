# pxasme

A single-pass x86_64 assembler that emits relocatable ELF64 objects.

## Usage

```bash
pxasme -Src myapp.asm -Dest myapp.o
ld -s -o myapp myapp.o
./myapp
```

## Status

- [X] Lexer
	- [X] "Variables" in data/rodata (u8, u16, u32, u64, sz)
		- [X] C-style addressing (bare or `&`)
	- [ ] Unified public/private, import/export syntax
	- [X] State machine parser for opcode arguments	
- [X] Parser
	- [X] mov, ret, syscall, sections, variables
	- [X] strlen($var) compile-time function
	- [ ] more isa ops
- [X] Code generation
	- [X] Generate ELF64 binaries
		- [X] Relocations
			- [X] `R_X86_64_32S` (n.b. not PIC)
			- [X] `R_X86_64_64`
	- [ ] Freestanding mode
