#!/bin/bash
# Usage: ./test_assemble.sh $'section .text\n mov rdx, 13370000000\n'

echo "$1" > src.asm
nasm -f elf64 src.asm
objdump -x src.o
objdump -D src.o
rm src.o
rm src.asm
